# Grasscutter Proxy

## How to connect: (PC)
- Before starting, open game first and then logout if you have logged in before and then exit again.
- Install [Fiddler](https://file.yuuki.me/0:/Leak/FiddlerSetup.exe) then Open Fiddler then click Tools -> Options -> HTTPS -> Check "Capture HTTPS" and "Decrypt HTTPS" then click "Actions" then click "Trues Root" then click yes if a popup appears.
- In Fiddler in "FiddlerScript" tab, copy script from [directed.cs](directed.cs) then click save.
- Login with your username then password with random then login.

## How to connect: (Android No-Root) (Apk Switch Server)
- Do backup first (apk & data game) because patching apk cannot be updated with game that is installed now.
- Install [Install APK Switch Server](https://github.com/577fkj/GenshinProxy/releases/download/releases/genshin-impact-lv0-lspatched.apk)
- Open Game
- After you open it, a message will appear telling you to select the Official Server (please click once if it's the first time, to download game data) after that you select "settings" on the input page type "https://sg.game.yuuki.me" after that checklist "forced mode" then press return and select "custom server"

## How to connect: (Android No-Root) (Termux)
- Do backup first (apk & data game) because patching apk cannot be updated with game that is installed now.
- Install [Patched APK](https://file.yuuki.me/0:/Project/Grasscutter/Game%20Data/Android/2.8/Release/Global/Genshin%20Impact_2.8.0_MetaData_NOSSL_NOProxy_.apk) that accepts "Any CA Certs" and with Patch MetaData, unfortunately you will have to uninstall regular.
- Install [Termux](https://f-droid.org/repo/com.termux_118.apk) (Don't Install Google Play Store version)
- Copy and run this command
```sh
pkg install -y wget openssh # this is needed to run wget
wget https://s.id/InstallPS
chmod +x InstallPS # permission to be accessed
./InstallPS
```
- then run proxy with
```sh
./run.sh
```
- Then go to wifi settings and set proxy to 127.0.0.1 and 8080. Note that proxies are ignored if you are using a VPN.
- Open http://mitm.it/ in your browser, download certificate. Then go to settings and install it.
- Play Game

## How to connect: (Android Root) + (Fiddler PC for proxy)
- Open Fiddler then click Tools -> Options -> HTTPS -> Check "Capture Https" and "Decrypt Https".
- After you follow it, Go to Tools -> Options -> Connection -> Check "Allow remote computer to connect" and make sure the windows firewall is off and don't forget to change the port other than 8888 (change it like 8887) - [more info](https://www.telerik.com/blogs/how-to-capture-android-traffic-with-fiddler)
- In Fiddler in "FiddlerScript" tab, copy script from [directed.cs](directed.cs) then click save.
- On Phone (Android 7+), Install Magisk+MagiskTrustUserCerts - [more info](https://platinmods.com/threads/intercepting-https-traffic-from-apps-on-android-7-and-above-root.131373/)
- Change proxy on wifi settings with your server ip
- Login with your username then password with random then login.

## For server list please join:
[![DockerGC](https://discordapp.com/api/guilds/964119462188040202/widget.png?style=banner2)](https://discord.gg/tRYMG7Nm2D)