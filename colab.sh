export TERM=cygwin

echo "Hello I'm Yuuki, Let's learn to install Grasscutter"

echo "Before starting we install git to clone repo and wget for download";
apt install -f git wget

echo "After that we install java jdk"
if [ ! -f "jdk-17_linux-x64_bin.deb" ]; then
 wget https://download.oracle.com/java/17/latest/jdk-17_linux-x64_bin.deb
 apt install -f ./jdk-17_linux-x64_bin.deb
 update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk-17/bin/java" 1
 update-alternatives --set java /usr/lib/jvm/jdk-17/bin/java
fi
java --version

echo "Ok now we have java now let's install database"
apt install -f mongodb
service mongodb start
mongod --version

echo "Remove file no need..."
apt autoremove

echo "Let's clone "
if [ ! -d "Grasscutter" ]; then
 git clone -b development https://github.com/Grasscutters/Grasscutter.git
fi

echo "Ok now we create Grasscutter jar file"
cd Grasscutter
if [ ! -f "grasscutter.jar" ]; then 
 ./gradlew
 ./gradlew jar
 cp grasscutter*.jar grasscutter.jar
fi

echo "Ok now it's good let's try clone Grasscutter Resources"
if [ ! -d "resources" ]; then
 git clone https://github.com/Koko-boya/Grasscutter_Resources
 mkdir resources
 cp -rf Grasscutter_Resources/Resources/* resources
 rm -R -f Grasscutter_Resources
fi

echo "We need Get Ip"
if [ ! -f "ngrok.yml" ]; then
 curl -s https://ngrok-agent.s3.amazonaws.com/ngrok.asc | \
      sudo tee /etc/apt/trusted.gpg.d/ngrok.asc >/dev/null && \
      echo "deb https://ngrok-agent.s3.amazonaws.com buster main" | \
      sudo tee /etc/apt/sources.list.d/ngrok.list && \
      sudo apt update && sudo apt install ngrok
fi  
wget --backups=1 https://gitlab.com/yukiz/grasscutter-proxy/-/raw/main/ngrok.yml
ngrok start --all --config ngrok.yml &

echo "OK Done"
java -jar grasscutter.jar