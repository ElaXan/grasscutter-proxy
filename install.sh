#!/bin/sh
export CARGO_BUILD_TARGET=aarch64-linux-android

echo "Update...."
apt update
apt -y full-upgrade
echo "Install Python and Rust"
pkg install -y python rustc-dev
echo "Install Mitmproxy"
# Install Bootstrapping pip installer
# m Flag corresponds to module-name
python3 -m ensurepip --upgrade
# Install pipx
python3 -m pip install --user pipx
python3 -m pipx ensurepath
~/.local/bin/pipx install mitmproxy

# Config Download
wget https://gitlab.com/yukiz/grasscutter-proxy/-/raw/main/proxy.py
wget https://gitlab.com/yukiz/grasscutter-proxy/-/raw/main/run.sh
chmod +x run.sh proxy.py
echo "Done, please type ./run.sh every time you want to run server proxy"